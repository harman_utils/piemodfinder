# pieModFinder

- A tool to return all python module imports and their version
- Calling the script within a directory will crawl the entire directory and return any `import module` calls from within .py or .ipynb scripts in that parent directory

**Notes**
- Modules that do not have the `__version__` attribute return `_NA_` for version
- Python scripts imported as modules return `_cust_` for version

An example of how to use the function by binding the call to a function within your `.bash_profile`

```bash
pieFinder(){
	python3 pieModFinder.py
}
```

### Output

```
python,3.7.3
sys,_NA_
os,_NA_
dis,_NA_
nbformat,4.4.0
importlib,_NA_
collections,_NA_
```
