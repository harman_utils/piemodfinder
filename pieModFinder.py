

"""
3/20/2020
Author: Gareth Harman

Script to return all used modules and their versions in a directory

"""

import os
import sys
from nbformat import read, NO_CONVERT
import dis
from collections import defaultdict
#from pprint import pprint
import importlib


####################################################################
# File parsers and tree walker
####################################################################

def findPies(filePath):
    
    """ Return python files in a dir """
    
    pyFiles = [] # Store python files
    
    # Walk directory
    for root, _, files in os.walk(filePath):
        if len(files) > 0:
            for ii in files:                
                if ii.split('.')[-1] in ['py', 'ipynb']:
                    pyFiles.append(root + '/' + ii)
                    
    return pyFiles
                    
def parsePy(filePath):
    
    """ Return raw text from a python file """
    
    with open(filePath) as f:
        content = f.read()
    
    return content

def parseJupyter(filePath, retRaw = True):
    
    """ Specific to parsing text from an .ipynb file """
    
    # Open jupyter notebook with correct formatting
    with open(filePath) as fp:
        notebook = read(fp, NO_CONVERT)
    
    # Extract the correct elements from the object
    cells = notebook.cells
    code_cells = [c for c in cells if c.cell_type == 'code']
    
    # Join all cells and then split by newline
    code_source = [c['source'] for c in code_cells]
    code_source = '\n'.join(code_source)
    
    # Split into lines
    code_lines = code_source.split('\n')
    
    # Return raw script
    if retRaw: return code_source
    
    # Otherwise return only imports
    imports = [x.split(' ')[1] for x in code_lines if x.split(' ')[0] == 'import']
    
    return imports

def gatherPies(pieList):
    
    """ Compile text from all .py files into string """
    
    pieTextFull = '' # Store full string
    
    for ii in pieList:
        if ii.split('.')[-1] == 'ipynb':
            pieTextFull += '\n' + parseJupyter(ii, True)
        else:
            pieTextFull += '\n' + parsePy(ii)
            
    # Fix tab and space issue
    return pieTextFull.replace('\t', '    ')

def pythonVersion():
    
    """ Return python version """
    
    sV = sys.version_info
    verFull = '.'.join([str(sV.major), 
                        str(sV.minor), 
                        str(sV.micro)])

    return 'python' + ',' + verFull


####################################################################
# Functions to return modules and version info
####################################################################

def returnImports(s):
    
    # Return module names
    moduleS = dis.get_instructions(s)
    imports = [x for x in moduleS if 'IMPORT' in x.opname]

    # Convert to dict
    moduleDict = defaultdict(list)
    
    # Get all imports
    for mod in imports:
        moduleDict[mod.opname].append(mod.argval)
    
    # Only get root import
    fullMod = [x.split('.')[0] for x in moduleDict['IMPORT_NAME']]
    rootMod = list(set(fullMod))
    
    return fullMod, rootMod

def returnVersion(imps, formatted = True, sep = ','):
    
    """ Return module and version """
    
    # Store mod name and version
    modDict = {'mod': [], 'ver': []}
    modStr = []
    
    # Iterate through all modules
    for mod in imps:
        try:
            # Load the module to get the version
            mymodule = importlib.import_module(mod)
            
            # Add version if it is available
            if hasattr(mymodule, '__version__'):
                ver = mymodule.__version__
            else:
                ver = '_NA_'

        # Exception for custom.py scripts
        except ModuleNotFoundError:
            ver = '_cust_'

        # Add items to lists
        modDict['mod'].append(mod)
        modDict['ver'].append(ver)
        modStr.append(mod + sep + ver)
        
    if formatted:
        return modStr
    else:
        return modDict


####################################################################
# Run
####################################################################

if __name__ == "__main__":
    
    assert len(sys.argv) in [1, 2], (
        print(f'Invalid number of arguments, expected [1 or 2], got {str(len(sys.argv))}'))
    
    if len(sys.argv) == 1:
        rootdir = os.getcwd()
    else:
        rootdir = sys.argv[1] # Argument to path root dir to scarpe
    
    # Check it exists 
    assert os.path.isdir(rootdir), (
        print(f'Invalid Directiory: {rootdir}'))

    # Find python files and gather them
    pyFiles = findPies(rootdir)
    
    # Make sure there are some python files in the dir
    assert pyFiles != None, (
        print(f'No .py/.ipynb files found in {rootdir}'))
    
    # Compile text
    pyText = gatherPies(pyFiles)

    # Return mod dirs
    modulesFull, modulesPart = returnImports(pyText)
    
    # Return model versions
    mods = returnVersion(modulesPart)   
    
    print(pythonVersion())
    for ii in mods: print(ii)

